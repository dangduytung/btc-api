import * as fs from 'fs';

export function createDirectory(path: string): void {
  // Create directory if it does not exist
  if (!fs.existsSync(path)) {
    fs.mkdirSync(path, { recursive: true });
  }
}

export function writeToFile(data: string, filePath: string): Promise<void> {
  return new Promise<void>((resolve, reject) => {
    fs.writeFile(filePath, data, (error) => {
      if (error) {
        reject(error);
      } else {
        resolve();
      }
    });
  });
}
