export interface BTC {
  mnemonic: string;
  privateKey: string;
  wifC: string;
  publicKeyC: string;
  addressC: string;
  wifU: string;
  publicKeyU: string;
  addressU: string;
}

export interface WalletBalance {
  address: string;
  balance: number;
}

export enum SeedType {
  PrivateKey = 'privateKey',
  Mnemonic = 'mnemonic',
  Passphrase = 'passphrase',
}

export enum EventTypes {
  BTC_BALANCE_POSITIVE = 'BTC_BALANCE_POSITIVE',
}
