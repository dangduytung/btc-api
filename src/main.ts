import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { LoggerService } from './logger.services';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    // logger: new LoggerService(),
  });
  await app.listen(process.env.PORT || 3000);
}
bootstrap();
