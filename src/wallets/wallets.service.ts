import { HttpService } from '@nestjs/axios';
import { Injectable, Logger } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';
import * as bip39 from 'bip39';
import * as bitcoin from 'bitcoinjs-lib';
import * as crypto from 'crypto';
import { ECPairFactory } from 'ecpair';
import { lastValueFrom } from 'rxjs';
import CryptoAccount from 'send-crypto';
import * as tinysecp from 'tiny-secp256k1';
import { FOLDER_DATA_BITCOIN_BALANCE } from '../config/constants';
import { BTC, EventTypes, SeedType, WalletBalance } from '../shards/types';
import { createDirectory, writeToFile } from '../utils/common.util';

@Injectable()
export class WalletsService {
  private readonly logger = new Logger(WalletsService.name);
  private readonly ECPair: any;
  private readonly MAIN_NET: boolean;
  private readonly NETWORK: any;

  constructor(
    private readonly httpService: HttpService,
    private readonly eventEmitter: EventEmitter2,
  ) {
    // Create directory if it does not exist
    createDirectory(FOLDER_DATA_BITCOIN_BALANCE);

    // Initialize
    this.ECPair = ECPairFactory(tinysecp);
    this.MAIN_NET = true;
    this.NETWORK = this.MAIN_NET
      ? bitcoin.networks.bitcoin
      : bitcoin.networks.testnet;

    // Event listeners
    this.eventEmitter.on(
      EventTypes.BTC_BALANCE_POSITIVE,
      this.saveBtcBalance.bind(this),
    );
  }

  async getBtcFromPrivateKey(privateKey: string): Promise<BTC> {
    const mnemonic = bip39.entropyToMnemonic(privateKey);
    const buffer = Buffer.from(privateKey, 'hex');

    /* Compressed */
    const keyPairC = this.ECPair.fromPrivateKey(buffer, {
      network: this.NETWORK,
    });
    const wifC = keyPairC.toWIF();
    const publicKeyC = keyPairC.publicKey.toString('hex');
    const addressC = bitcoin.payments.p2pkh({
      pubkey: keyPairC.publicKey,
      network: this.NETWORK,
    }).address;

    /* Uncompressed */
    const keyPairU = this.ECPair.fromPrivateKey(buffer, {
      compressed: false,
      network: this.NETWORK,
    });
    const wifU = keyPairU.toWIF();
    const publicKeyU = keyPairU.publicKey.toString('hex');
    const addressU = bitcoin.payments.p2pkh({
      pubkey: keyPairU.publicKey,
      network: this.NETWORK,
    }).address;

    const btc: BTC = {
      mnemonic,
      privateKey,
      wifC,
      publicKeyC,
      addressC,
      wifU,
      publicKeyU,
      addressU,
    };
    // this.logger.log(`btc: ${JSON.stringify(btc)}`);
    return btc;
  }

  async getBtcBalance(address: string): Promise<WalletBalance> {
    this.logger.log(`address: ${address}`);
    try {
      const response = await lastValueFrom(
        this.httpService.get(`https://blockchain.info/rawaddr/${address}`),
      );
      const data = response.data;
      this.logger.debug(`data: ${JSON.stringify(data)}`);
      this.logger.log(`final_balance ${data.final_balance}`);
      const balance = data.final_balance / 100000000; // Convert satoshis to BTC
      return { address, balance };
    } catch (error) {
      this.logger.error(`Error fetching balance: ${error}`);
      throw error;
    }
  }

  async getBtcBalanceByPrivateKey(privateKey: string): Promise<WalletBalance> {
    try {
      this.logger.log(`privateKey: ${privateKey}`);
      const wallet = new CryptoAccount(privateKey);
      const address = await wallet.address('BTC');
      const balance = await wallet.getBalance('BTC');
      this.logger.log(
        `privateKey: ${privateKey}, address: ${address}, balance: ${balance}`,
      );
      return { address, balance };
    } catch (error) {
      this.logger.error(
        `Error getting balance for private key ${privateKey}: ${error}`,
      );
      throw error;
    }
  }

  async generateByType(
    seedType: SeedType,
    seed: string,
    checkBalance: boolean = false,
  ): Promise<any> {
    // this.logger.log(`seedType: ${seedType}, seed: ${seed}`);
    if (!seed) {
      throw new Error('Seed is required');
    }
    let privateKey: string;
    switch (seedType) {
      case SeedType.PrivateKey:
        privateKey = seed;
        break;
      case SeedType.Mnemonic:
        privateKey = await this.getPrivateKeyFromMnemonic(seed);
        break;
      case SeedType.Passphrase:
        privateKey = bitcoin.crypto.sha256(Buffer.from(seed)).toString('hex');
        break;
      default:
        throw new Error('Invalid seed type');
    }
    const btc = privateKey ? await this.getBtcFromPrivateKey(privateKey) : null;
    if (!btc) {
      return null;
    }

    // Check balance
    if (checkBalance) {
      const walletBalance: WalletBalance = await this.getBtcBalance(
        btc.addressC,
      );
      const result = {
        btc,
        walletBalance,
      };
      this.logger.log(`result: ${JSON.stringify(result)}`);

      // Emit event
      if (walletBalance.balance > 0) {
        this.eventEmitter.emit(EventTypes.BTC_BALANCE_POSITIVE, {
          btc,
          walletBalance,
        });
      }
      return result;
    }
    return btc;
  }

  async generateBtcRandom(
    seedType: SeedType = SeedType.PrivateKey,
    checkBalance: boolean = false,
  ): Promise<any> {
    let seed: string;
    switch (seedType) {
      case SeedType.PrivateKey:
        seed = crypto.randomBytes(32).toString('hex');
        break;
      case SeedType.Mnemonic:
        seed = bip39.generateMnemonic(256);
        break;
      case SeedType.Passphrase:
        seed = crypto.randomBytes(16).toString('base64'); // Generate 16 random bytes and encode to base64
        break;
      default:
        throw new Error('Invalid seed type');
    }
    const btc = await this.generateByType(seedType, seed);
    if (!btc) {
      return null;
    }

    // Check balance
    if (checkBalance) {
      const walletBalance: WalletBalance = await this.getBtcBalance(
        btc.addressC,
      );
      const result = {
        btc,
        walletBalance,
      };
      this.logger.log(`result: ${JSON.stringify(result)}`);

      // Emit event
      if (walletBalance.balance > 0) {
        this.eventEmitter.emit(EventTypes.BTC_BALANCE_POSITIVE, {
          btc,
          walletBalance,
        });
      }
      return result;
    }
    return btc;
  }

  async getPrivateKeyFromMnemonic(mnemonic: string): Promise<string> {
    const isValidMnemonic: boolean = bip39.validateMnemonic(mnemonic);
    // this.logger.log(`isValidMnemonic ${isValidMnemonic}`);
    if (!isValidMnemonic) {
      // throw new Error('Invalid mnemonic');
      return null;
    }
    const privateKey: string = bip39.mnemonicToEntropy(mnemonic);
    return privateKey;
  }

  async saveBtcBalance(data: any): Promise<void> {
    const { btc, walletBalance } = data;
    // console.log('btc', btc);
    // console.log('walletBalance', walletBalance);
    // Write log
    this.logger.log(
      `btc: ${JSON.stringify(btc)}, walletBalance: ${JSON.stringify(
        walletBalance,
      )}`,
    );

    // Write bitcoin wallet to file
    const filePath = `${FOLDER_DATA_BITCOIN_BALANCE}/wallet_privateKey-${btc.privateKey}_address-${btc.addressC}_balance-${walletBalance.balance}_${Date.now()}.json`;
    writeToFile(JSON.stringify({ ...btc, ...walletBalance }), filePath);
  }

  generateRandomMnemonicTest(): string {
    const wordList: string[] = bip39.wordlists.english; // Use the English word list
    const mnemonics: string[] = [];

    // for (let i = 0; i < 24; i++) {
    //   const randomIndex: number = Math.floor(Math.random() * wordList.length);
    //   mnemonics.push(wordList[randomIndex]);
    // }

    // const mnemonic = mnemonics.join(' ');
    // this.logger.log(`mnemonic: ${mnemonic}`);
    // return mnemonic;

    // Step 1: Generate 23 random words
    for (let i = 0; i < 23; i++) {
      const randomIndex: number = Math.floor(Math.random() * wordList.length);
      mnemonics.push(wordList[randomIndex]);
    }

    // Step 2: Calculate checksum
    // const mnemonicWithoutChecksum = mnemonics.join(' ');
    // const entropy = BIP39.mnemonicToEntropy(mnemonicWithoutChecksum);
    // const checksum = BIP39.mnemonicToChecksumBits(mnemonicWithoutChecksum);

    // // Step 3: Append checksum
    // const mnemonicWithChecksum = mnemonicWithoutChecksum + ' ' + checksum;

    // this.logger.log(`mnemonic: ${mnemonicWithChecksum}`);

    // // Step 4: Return the mnemonic
    // return mnemonicWithChecksum;

    // Step 2: Calculate checksum
    const entropy = Buffer.from(mnemonics.join(' '), 'utf8');
    const hash = crypto.createHash('sha256').update(entropy).digest();
    const checksumBits = hash[0] >> 3; // Take the first byte and use the first 5 bits
    const checksumIndex = checksumBits % wordList.length;
    mnemonics.push(wordList[checksumIndex]);

    // Step 3: Return the mnemonic
    const mnemonic = mnemonics.join(' ');
    this.logger.log(`mnemonic: ${mnemonic}`);
    return mnemonic;
  }
}
