import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { SeedType } from '../shards/types';
import { WalletsService } from './wallets.service';

@Controller('wallets')
export class WalletsController {
  constructor(private readonly walletsService: WalletsService) {}

  @Get('balance/:address')
  async getBalance(
    @Param('address') address: string,
  ): Promise<{ address: string; balance: number }> {
    return await this.walletsService.getBtcBalance(address);
  }

  @Get('privateKey/:privateKey')
  async getBtcBalanceByPrivateKey(
    @Param('privateKey') privateKey: string,
  ): Promise<{ address: string; balance: number }> {
    return await this.walletsService.getBtcBalanceByPrivateKey(privateKey);
  }

  @Post('generate-random-wallet')
  async generateRandomWallet(
    @Body() body: { type?: SeedType; checkBalance?: boolean },
  ) {
    const { type, checkBalance } = body;
    return await this.walletsService.generateBtcRandom(type, checkBalance);
  }

  @Post('generate-by-type')
  async generateByType(
    @Body() body: { type: SeedType; seed: string; checkBalance?: boolean },
  ) {
    const { type, seed, checkBalance } = body;
    return await this.walletsService.generateByType(type, seed, checkBalance);
  }
}
